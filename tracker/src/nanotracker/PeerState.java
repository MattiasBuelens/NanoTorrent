package nanotracker;

public enum PeerState {
	UNKNOWN, STARTED, STOPPED, COMPLETED
}
